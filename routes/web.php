<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Get Table Here
Route::get('/GetTableMahasiswa','DataTableController@GetTableMahasiswa');
Route::get('/GetTableMatakuliahData','DataTableController@GetTableMatakuliahData');



// Get Data Here
// Route::get('/GetMataKuliah','GetDataController@GetMataKuliah');
Route::get('/srcByMataKuliah/{id}','GetDataController@srcByMataKuliah');




// Get Dynamic Web Here
 Route::get('/pageMahasiswa','WebController@pageMahasiswa');
 Route::get('/pageMatakuliah','WebController@pageMatakuliah');


//  POST Here you 
Route::post('/createMahasiswa','PostDataController@createMahasiswa');
Route::post('/editMahasiswa','PostDataController@editMahasiswa');
Route::get('/hapusMahasiswa/{idMahasiswa}','PostDataController@hapusMahasiswa');

Route::post('/createMatakuliah','PostDataController@createMatakuliah');
Route::post('/editMatakuliah','PostDataController@editMatakuliah');
Route::get('/hapusMatakuliah/{idMatakuliah}','PostDataController@hapusMatakuliah');