<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    <title>TEST 5000 DATA</title>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">TEST 5000 DATA </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/pageMahasiswa">Mahasiswa</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/pageMatakuliah">Mata Kuliah</a>
            </li>
            {{-- <li class="nav-item">
              <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li> --}}
          </ul>
        </div>
      </nav>
      @if (session('success'))
      <div class="col-sm-12 mt-5">
          <div class="alert  alert-success alert-dismissible fade show" role="alert">
              <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @elseif (session('edit'))
      <div class="col-sm-12 mt-5">
          <div class="alert  alert-primary alert-dismissible fade show" role="alert">
              <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @elseif (session('delete'))
      <div class="col-sm-12 mt-5">
          <div class="alert  alert-danger alert-dismissible fade show" role="alert">
              <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @endif
      <div class="card mt-5">
        <div class="card-header">
          <h5>Mahasiswa Table
            <button class="btn btn-info btn-sm float-right" id="" data-toggle="modal" data-target="#modalCreateData">Tambah</button>
            <div class="form-group col-md-12 mt-3">
              <select class="custom-select " id="srcByMataKuliah" name="mataKuliahNew" required>
                <option value=" "> -- Pilih Mata Kuliah --</option>
                @foreach($getMataKuliah as $item)
                  <option value="{{$item->idMatakuliah}}">{{$item->nama_matakuliah}}</option>
                @endforeach
              </select>
            </div>
          </h5>
        </div>
        <div class="card-body">
          <table class="table" id="tableMahasiswa">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">MahasiswaID</th>
                <th scope="col">NIK</th>
                <th scope="col">Nama Mahasiswa</th>                
                <th scope="col">Mata Kuliah</th>
                <th scope="col">id Kuliah</th>
                <th scope="col">Action</th>                
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>

    </div>

    {{-- Modal Create Mahasiswa --}}
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" id="modalCreateData" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Data Mahasiswa</h5>
            {{-- <div class="form-group float-right">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="checkCreate">
                <label class="form-check-label" for="checkCreate">
                  Create Data Mahasiswa
                </label>
              </div>
            </div> --}}
          </div>
          <div class="modal-body">
            <form action="/createMahasiswa" method="post">
              @csrf
              <input type="hidden" readonly id="idMahasiswaNew" name="idMahasiswaNew" required>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="inputEmail4">NIK</label>
                  <input type="num" min="1" id="nikMahasiswaNew"class="form-control" name="nikMahasiswaNew" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="namaMahasiswa">Nama Mahasiswa</label>
                  <input type="text" class="form-control" id="namaMahasiswaNew" name="namaMahasiswaNew" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="mataKuliah">Mata Kuliah</label>
                  <select class="custom-select " id="mataKuliahNew" name="mataKuliahNew" required>
                    <option value=" "> -- Pilih Mata Kuliah --</option>
                    @foreach($getMataKuliah as $item)
                      <option value="{{$item->idMatakuliah}}">{{$item->nama_matakuliah}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              
              <button type="submit" class="btn btn-success float-right">Tambah Baru</button>
              <button class="btn  float-right mr-3" data-dismiss="modal">Tutup</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal Edit Mahasiswa --}}
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" id="modalEditData" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Data Mahasiswa</h5>
            <div class="form-group float-right">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="checkEdit">
                <label class="form-check-label" for="checkEdit">
                  Edit Data Mahasiswa
                </label>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <form action="/editMahasiswa" method="post">
              @csrf
              <input type="hidden" readonly id="idMahasiswa" name="idMahasiswa" required>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="inputEmail4">NIK</label>
                  <input type="num" min="1" id="nikMahasiswa"class="form-control" name="nikMahasiswa" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="namaMahasiswa">Nama Mahasiswa</label>
                  <input type="text" class="form-control" id="namaMahasiswa" name="namaMahasiswa" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="mataKuliah">Mata Kuliah</label>
                  <select class="custom-select " id="mataKuliah" name="mataKuliah" required>
                    <option value=" "> -- Pilih Mata Kuliah --</option>
                    @foreach($getMataKuliah as $item)
                      <option value="{{$item->idMatakuliah}}">{{$item->nama_matakuliah}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              
              <button type="submit" class="btn btn-success float-right" id="btnEdit">Edit</button>
              <button class="btn  float-right mr-3" data-dismiss="modal">Tutup</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script>
      $(document).ready( function () {
        let table = $('#tableMahasiswa').DataTable(
          {
            "ordering": false,
            "info":     false,
            "bFilter": false,
            "bLengthChange": false,
            "processing": true,
            ajax : {
              url : '/GetTableMahasiswa',
              type : 'get',
            },
            "columns" : [
              {'data' : 'no'},
              {'data' : 'idMahasiswa'},
              {'data' : 'nikMahasiswa'},
              {'data' : 'namaMahasiswa'},
              {'data' : 'mataKuliah'},
              {'data' : 'idMatakuliah'},
            ],
            "columnDefs" : [{
              "targets": 0,
              class : 'text-center',
              //"visible" : false                
              },{
              "targets": 1,
              class : 'text-center',
              "visible" : false                
              },{
              "targets": 2,
              class : 'text-center',
              //"visible" : false                
              },{
              "targets": 3,
              class : 'text-center',
              //"visible" : false                
              },{
              "targets": 4,
              class : 'text-center',
              //"visible" : false                
              },{
              "targets": 5,
              class : 'text-center',
              "visible" : false                
              },{
              "targets": 6,
              // "data": '',
              render:function(data,type,row,meta){
                  return '<button class="btn btn-info btn-sm" id="detailMahasiswa" data-toggle="modal" data-target="#modalEditData">Detail</button> '+
                         ' <button class="btn btn-danger btn-sm" id="hapusMahasiswa" >Hapus</button>';  
                }                
              }

            ]
          }
        );
          
        $('#srcByMataKuliah').change(function(){
              // table.destroy();	
              // table.destroy();
          let id = $('#srcByMataKuliah').val();
          let table = $('#tableMahasiswa').DataTable();
          table.ajax.url( "/srcByMataKuliah/"+id ).load();

        });
        $('#tableMahasiswa tbody').on('click','#detailMahasiswa', function(){
              $('#idMahasiswa').val('');
              $('#nikMahasiswa').val('');
              $('#namaMahasiswa').val('');
              $('#mataKuliah').val('');
              $('#checkEdit').prop('checked',false);
              $('#btnEdit').hide();
                  var data = table.row( $(this).parents('tr') ).data();
                      $('#idMahasiswa').val(data.idMahasiswa);
                      $('#nikMahasiswa').val(data.nikMahasiswa).prop('disabled', true);
                      $('#namaMahasiswa').val(data.namaMahasiswa).prop('disabled', true);              
                      $('#mataKuliah').val(data.idMatakuliah).prop('disabled', true).attr('selected', 'selected');
                      $('#checkEdit').on('click', function(){
                        if($(this).is(":checked")){
                          $('#nikMahasiswa').prop('disabled', false );
                          $('#namaMahasiswa').prop('disabled', false  );
                          $('#mataKuliah').prop('disabled', false );
                          $('#btnEdit').show();
                        }
                        else if($(this).is(":not(:checked)")){
                          $('#nikMahasiswa').val(data.nikMahasiswa).prop('disabled', true);
                          $('#namaMahasiswa').val(data.namaMahasiswa).prop('disabled', true);
                          $('#mataKuliah').val(data.idMatakuliah).prop('disabled', true);
                          $('#btnEdit').hide();
                        }
                      });
          
        });
        
        $('#tableMahasiswa tbody').on('click','#hapusMahasiswa', function(){
          var data = table.row( $(this).parents('tr') ).data();
          if (confirm('Apakah anda ingin menghapus NIK '+data.nikMahasiswa+' ini ?')) {
              url = "/hapusMahasiswa/"+data.idMahasiswa;
              window.location = url;
          } else {

          }
        });



      });
    </script>
  </body>
</html>
