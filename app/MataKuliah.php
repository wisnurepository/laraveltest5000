<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
  protected $table = 'mata_kuliahs';
  protected $primaryKey = 'idMatakuliah';
}
