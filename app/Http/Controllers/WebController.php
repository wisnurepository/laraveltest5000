<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\MataKuliah;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class WebController extends Controller
{
  
  public function pageMahasiswa()
  {
    $getMataKuliah = MataKuliah::get(); 
    return view('pageMahasiswa',compact('getMataKuliah'));
  }

  public function pageMatakuliah()
  {
    return view('pageMatakuliah');
  }
}
