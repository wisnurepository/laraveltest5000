<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\MataKuliah;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class PostDataController extends Controller
{
  public function createMahasiswa(Request $request)
  {

    $dataMahasiswa = new Mahasiswa;
    $dataMahasiswa->nik = $request->nikMahasiswaNew;
    $dataMahasiswa->nama_mahasiswa = $request->namaMahasiswaNew;
    $dataMahasiswa->matakuliahId = $request->mataKuliahNew;
    $dataMahasiswa->save();

    return redirect()->back()->with('success','Menambahkan data NIK : '.$request->nikMahasiswaNew.' berhasil disimpan');
    
  }


  public function editMahasiswa(Request $request)
  {
    $key = Crypt::decryptString($request->idMahasiswa);
    // dd($key);
    $getMahasiswa = Mahasiswa::where('id',$key)
                             ->first();
    // dd($getMahasiswa
    $getMahasiswa->nik = $request->nikMahasiswa;
    $getMahasiswa->nama_mahasiswa = $request->namaMahasiswa;
    $getMahasiswa->matakuliahId = $request->mataKuliah;
    $getMahasiswa->update();
    
    return redirect()->back()->with('success','Update data NIK : '.$request->nikMahasiswa.' berhasil ');
  }

  public function hapusMahasiswa($idMahasiswa)
  {
    $key = Crypt::decryptString($idMahasiswa);
    $getMahasiswa = Mahasiswa::where('mahasiswas.id',$key)
                             ->first();
    $getMahasiswa->delete();    
    return redirect()->back()->with('delete','Delete data NIK berhasil ');
  }


  // Mata Kuliah Here

  
  public function createMatakuliah(Request $request)
  {
    $getMatakuliah = new Matakuliah;
    $getMatakuliah->nama_matakuliah = $request->namaMatakuliahNew;
    $getMatakuliah->save(); 
    return redirect()->back()->with('success','Menambahkan data Mata Kuliah : '.$request->namaMatakuliahNew.' berhasil disimpan');
  }

  public function editMatakuliah(Request $request)
  {
    $key = Crypt::decryptString($request->idMatakuliah);
    // dd($key);
    $getMatakuliah = MataKuliah::where('idMatakuliah',$key)->first();

    $getMatakuliah->nama_matakuliah = $request->namaMatakuliah;
    $getMatakuliah->update();

    return redirect()->back()->with('success','Update data Mata Kuliah : '.$getMatakuliah->nama_matakuliah.' berhasil ');
  }

  public function hapusMatakuliah($idMatakuliah)
  {
    $key = Crypt::decryptString($idMatakuliah);
    $getMahasiswa = Mahasiswa::leftjoin('mata_kuliahs','mata_kuliahs.idMatakuliah','matakuliahId')
                             ->where('mahasiswas.matakuliahId',$key)
                             ->get();
    for($i=0; $i < count($getMahasiswa);$i++)
    {
      $getMahasiswa[$i]->matakuliahId = null;
      $getMahasiswa[$i]->update();
    }
    $getMatakuliah = MataKuliah::where('idMatakuliah',$key)->first();
    $getMatakuliah->delete();
        
    return redirect()->back()->with('delete','Delete data Mata Kuliah berhasil ');
  }


}
