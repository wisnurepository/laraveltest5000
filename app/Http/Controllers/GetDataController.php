<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\MataKuliah;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;


class GetDataController extends Controller
{


  public function srcByMataKuliah($id)
  {
    $getMahasiswa = Mahasiswa::leftjoin('mata_kuliahs','mata_kuliahs.idMatakuliah','matakuliahId')
                             ->where('mata_kuliahs.idMatakuliah',$id)
                            //  ->limit(25)
                            //  ->where('mata_kuliahs.nama_matakuliah','Teknik Informatika')
                             ->get(
                                    [
                                      'mahasiswas.id',
                                      'mahasiswas.nik',
                                      'mahasiswas.nama_mahasiswa',
                                      'mata_kuliahs.idMatakuliah',
                                      'mata_kuliahs.nama_matakuliah'
                                    ]
                                  );

    $data = array();
    $no = 1;
    foreach($getMahasiswa as $dataEach)
    {
        $column['no'] = (string)$no++;
        $column['idMahasiswa'] = (string)Crypt::encryptString($dataEach->id);
        $column['namaMahasiswa'] = (string)$dataEach->nama_mahasiswa;
        $column['nikMahasiswa'] = (string)$dataEach->nik;
        $column['idMatakuliah'] = (string)$dataEach->idMatakuliah;
        $column['mataKuliah'] = (string)$dataEach->nama_matakuliah;
        $data[] = $column;
    }

    if($data)
    {
      $response = [
                'data' => $data,
                'status' => 'Success Data',
                'kode' => '001',
              ];
    }
    elseif(!$data)
    {
      $response = [
                // 'data' => $data,
                'status' => 'Empty Data',
                'kode' => '002',
              ];

    }
    else
    {
      $response = [
                // 'data' => $data,
                'status' => 'Error Data',
                'kode' => '003',
              ];        
    }


    return response()->json($response);
  }


}
