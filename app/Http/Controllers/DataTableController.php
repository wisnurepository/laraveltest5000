<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\MataKuliah;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class DataTableController extends Controller
{

  //  Get Table Data Mahasiswa and Matakuliah
  public function GetTableMahasiswa()
  {
      $getMahasiswa = Mahasiswa::leftjoin('mata_kuliahs','mata_kuliahs.idMatakuliah','matakuliahId')
                              //  ->limit(25)
                              //  ->where('mata_kuliahs.nama_matakuliah','Teknik Informatika')
                               ->get(
                                      [
                                        'mahasiswas.id',
                                        'mahasiswas.nik',
                                        'mahasiswas.nama_mahasiswa',
                                        'mata_kuliahs.idMatakuliah',
                                        'mata_kuliahs.nama_matakuliah'
                                      ]
                                    );

      $data = array();
      $no = 1;
      foreach($getMahasiswa as $dataEach)
      {
          $column['no'] = (string)$no++;
          $column['idMahasiswa'] = (string)Crypt::encryptString($dataEach->id);
          $column['namaMahasiswa'] = (string)$dataEach->nama_mahasiswa;
          $column['nikMahasiswa'] = (string)$dataEach->nik;
          $column['idMatakuliah'] = (string)$dataEach->idMatakuliah;
          $column['mataKuliah'] = (string)$dataEach->nama_matakuliah;
          $data[] = $column;
      }

      if($data)
      {
        $response = [
                  'data' => $data,
                  'status' => 'Success Data',
                  'kode' => '001',
                ];
      }
      elseif(!$data)
      {
        $response = [
                  // 'data' => $data,
                  'status' => 'Empty Data',
                  'kode' => '002',
                ];

      }
      else
      {
        $response = [
                  // 'data' => $data,
                  'status' => 'Error Data',
                  'kode' => '003',
                ];        
      }


      return response()->json($response);
  }

  // Get Table Data Mahasiswa
  public function GetTableMahasiswaData()
  {
      $getMahasiswa = Mahasiswa::get(
                                      [
                                        'mahasiswas.id',
                                        'mahasiswas.nik',
                                        'mahasiswas.nama_mahasiswa',
                                      ]
                                    );

      $data = array();
      $no = 1;
      foreach($getMahasiswa as $dataEach)
      {
          $column['no'] = (string)$no++;
          $column['idMahasiswa'] = (string)Crypt::encryptString($dataEach->id);;
          $column['namaMahasiswa'] = (string)$dataEach->nama_mahasiswa;
          $column['nikMahasiswa'] = (string)$dataEach->nik;
          $data[] = $column;
      }

      if($data)
      {
        $response = [
                  'data' => $data,
                  'status' => 'Success Data',
                  'kode' => '001',
                ];
      }
      elseif(!$data)
      {
        $response = [
                  // 'data' => $data,
                  'status' => 'Empty Data',
                  'kode' => '002',
                ];

      }
      else
      {
        $response = [
                  // 'data' => $data,
                  'status' => 'Error Data',
                  'kode' => '003',
                ];        
      }


      return response()->json($response);
  }

  
  // Get Table Data Mahasiswa
  public function GetTableMatakuliahData()
  {
      $getMataKuliah = MataKuliah::get(
                                      [
                                        'mata_kuliahs.idMatakuliah',
                                        'mata_kuliahs.nama_matakuliah'

                                      ]
                                    );

      $data = array();
      $no = 1;
      foreach($getMataKuliah as $dataEach)
      {
          $column['no'] = (string)$no++;
          $column['idMataKuliah'] = (string)Crypt::encryptString($dataEach->idMatakuliah);
          $column['namaMatakuliah'] = (string)$dataEach->nama_matakuliah;
          $data[] = $column;
      }

      if($data)
      {
        $response = [
                  'data' => $data,
                  'status' => 'Success Data',
                  'kode' => '001',
                ];
      }
      elseif(!$data)
      {
        $response = [
                  // 'data' => $data,
                  'status' => 'Empty Data',
                  'kode' => '002',
                ];

      }
      else
      {
        $response = [
                  // 'data' => $data,
                  'status' => 'Error Data',
                  'kode' => '003',
                ];        
      }


      return response()->json($response);
  }


}
