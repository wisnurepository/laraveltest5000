<?php

use Illuminate\Database\Seeder;

use App\MataKuliah;

class MataKuliahSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $create = [
        ['nama_matakuliah' => 'Matematika'],
        ['nama_matakuliah' => 'Sistem Informasi'],
        ['nama_matakuliah' => 'Teknik Informatika'],
        ['nama_matakuliah' => 'DKV'],
        ['nama_matakuliah' => 'Sastra Indonesia'],
      ];

      foreach ($create as $item) {
         MataKuliah::create($item);
      }
    }
}
