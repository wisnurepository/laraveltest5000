<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Mahasiswa;
use Faker\Generator as Faker;

$factory->define(Mahasiswa::class, function (Faker $faker) {
    static $nik=1;
    return [
      'nik' => 1010012020+$nik++,
      'nama_mahasiswa'         =>      $faker->name,
      'matakuliahId'       =>      function () {
                              return App\Matakuliah::inRandomOrder()->first()->idMatakuliah;
      }
    ];
});
